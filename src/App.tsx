import React from "react";
import {
  Switch,
  Route,
  Redirect,
  BrowserRouter as Router,
} from "react-router-dom";
import "./App.scss";
import HomePage from "./containers/HomePage";
import MovieContextProvider from "./contexts/MovieContext";
import MovieDetailsPage from "./containers/MovieDetailPage";
import Header from "./components/Header";
import { SearchPage } from "./containers/SearchPage";
import { SearchContextProvider } from "./contexts/SearchContext";

function App() {
  return (
    <Router>
      <Header />
      <MovieContextProvider>
        <Switch>
          <Route exact path="/">
            <Redirect from="/" to="/home" />
          </Route>
          <Route path="/home">
            <HomePage />
          </Route>
          <Route path="/movie/:id">
            <MovieDetailsPage />
          </Route>
          <Route path="/search">
            <SearchContextProvider>
              <SearchPage />
            </SearchContextProvider>
          </Route>
        </Switch>
      </MovieContextProvider>
    </Router>
  );
}

export default App;
