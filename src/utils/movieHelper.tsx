const IMAGE_BASE_URL = "https://image.tmdb.org/t/p";
export const getMovieImageUrl = (imagePath: string, size: string | number = 'original') : string => {
  return `${IMAGE_BASE_URL}/${size}${imagePath}`;
}
