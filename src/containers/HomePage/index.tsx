import React, { useState, useContext } from "react";
import { MovieContext } from "../../contexts/MovieContext";
import {
  Container,
  Input,
  Button,
  Row,
  Col,
} from "reactstrap";
import { useHistory } from "react-router-dom";
import ListMovies from "../../components/ListMoves";

export default function HomePage() {
  const { movies } = useContext(MovieContext);
  const [searchKey, setSearchKey] = useState("");
  const history = useHistory();

  const handleSearch = () => {
    if (searchKey === "") {
      return;
    }
    history.push(`/search?query=${searchKey}`);
  };

  const handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      handleSearch();
    }
  }
  return (
    <>
      <section>
        <div className="main-banner">
          <Container>
            <h2>Welcome.</h2>
            <h3>
              Millions of movies, TV shows and people to discover. Explore now
            </h3>
            <Row className="mt-4">
              <Col>
                <Input
                  type="text"
                  name="search"
                  id="search"
                  value={searchKey}
                  placeholder="Search for movie.... "
                  onKeyDown={handleKeyDown}
                  onChange={(e) => {
                    setSearchKey(e.target.value);
                  }}
                />
              </Col>
              <Col xs="auto">
                <Button data-test="btn-search" className="col-auto" onClick={handleSearch}>
                  Search
                </Button>
              </Col>
            </Row>
          </Container>
        </div>
      </section>
      <Container className="my-4">
        <h2 className="mb-4">Popular Movie</h2>
        <ListMovies movies={movies} />
      </Container>
    </>
  );
}
