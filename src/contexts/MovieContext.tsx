import React, { createContext, useEffect, useState } from "react";
import request from "../utils/request";
import { IMovie } from "../interfaces/MovieInterface";

export const MovieContext = createContext({
  movies: null as Array<IMovie> | null,
  movie: null as IMovie | null,
  getMovie: null as any,
  clearMovie: null as any,
  video: "",
  similarMovies: null as Array<IMovie> | null,
});

interface MoviesResponse {
  results: Array<IMovie>;
}

export const MovieContextProvider = (props) => {
  const apiKey: string = "?api_key=23cfaee354d5ffc6f4e1d33ea4312ce2";
  const baseURL: string = "https://api.themoviedb.org/3";
  const movieKinds = {
    POPULAR: "popular",
    // UPCOMMING: 'upcmming'
  };
  const [movies, setMovies] = useState([] as Array<IMovie>);
  const [movie, setMovie] = useState(null as IMovie | null);
  const [video, setVideo] = useState("");
  const [similarMovies, setSimularMovies] = useState(
    null as Array<IMovie> | null
  );

  useEffect(() => {
    const popularUrl = `${baseURL}/movie/${movieKinds.POPULAR}${apiKey}`;

    request(popularUrl).then((response: MoviesResponse) => {
      setMovies(response.results);
    });
  }, [movieKinds.POPULAR]);

  const getMovie = (movieId: number) => {
    const movieUrl = `${baseURL}/movie/${movieId}${apiKey}`;
    const videoUrl = `${baseURL}/movie/${movieId}/videos${apiKey}`;
    const similarMoviesUrl = `${baseURL}/movie/${movieId}/similar${apiKey}`;

    request(movieUrl).then((response: any) => {
      setMovie(response);
    });

    request(videoUrl).then((response: any) => {
        setVideo(response.results[0].key);
    });
    request(similarMoviesUrl).then((response: any) => {
      setSimularMovies(response.results);
    });
  };

  const clearMovie = () => {
    setMovie(null);
    setVideo("");
    setSimularMovies(null);
  };

  return (
    <MovieContext.Provider
      value={{ movies, movie, getMovie, clearMovie, video, similarMovies }}
    >
      {props.children}
    </MovieContext.Provider>
  );
};

export default MovieContextProvider;
