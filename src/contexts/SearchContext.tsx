import React, { useState, createContext } from "react";
import { IMovie } from "../interfaces/MovieInterface";
import request from "../utils/request";

export const SearchContext = createContext({
  searchResults: null as Array<IMovie> | null,
  search: function(query: string) {}
});

export const SearchContextProvider = (props) => {
  const apiKey: string = "?api_key=23cfaee354d5ffc6f4e1d33ea4312ce2";
  const baseURL: string = "https://api.themoviedb.org/3";

  const [searchResults, setSearchResults] = useState(null)
  const search = (query: string) => {
    const searchUrl = `${baseURL}/search/multi${apiKey}${query}`;

    request(searchUrl).then((response: any) => {
      setSearchResults(response.results);
    });
  };
  return (
    <SearchContext.Provider value={{ searchResults, search }}>
      {props.children}
    </SearchContext.Provider>
  );
};
