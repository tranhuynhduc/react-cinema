import React, {
  useContext,
  useEffect,
  useState,
} from "react";
import { MovieContext } from "../../contexts/MovieContext";
import { useParams } from "react-router-dom";
import { getMovieImageUrl } from "../../utils/movieHelper";
import { Container, Row, Col, Button, Modal, ModalBody } from "reactstrap";
import { Link } from "react-router-dom";
import ListMovies from "../../components/ListMoves";
import { Loading } from "../../components/Loading";

export default function MovieDetailsPage() {
  const { getMovie, clearMovie, movie, video, similarMovies } = useContext(
    MovieContext
  );
  const { id } = useParams();

  const [modal, setModal] = useState(false);
  const toggle = () => {
    setModal(!modal)
  };

  useEffect(() => {
    getMovie(id);
    return () => {
      return clearMovie();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  const renderVideo = () => {
    return (
      <div className="my-3">
        <Button color="danger" data-testid="btn-trailer" onClick={toggle}>
          Play Trailer
        </Button>
      </div>
    );
  };

  const renderVideoModal = () => {
    return (
      <div>
        <Modal
          isOpen={modal}
          toggle={toggle}
          size="lg"
          centered
          data-testid="modal-trailler"
        >
          <ModalBody>
            <div className="embed-responsive embed-responsive-16by9">
              <iframe
                className="embed-responsive-item"
                src={`//www.youtube.com/embed/${video}`}
                title="video"
              ></iframe>
            </div>
          </ModalBody>
        </Modal>
      </div>
    );
  };

  if (movie === null) {
    return <Loading />;
  }
  const {
    backdrop_path,
    // genre_ids,
    // original_language,
    // original_title,
    overview,
    // popularity,
    poster_path,
    release_date,
    title,
    // video,
    // vote_average,
    // vote_count,
    genres = [],
    runtime,
  } = movie;

  return (
    <>
      <div
        data-testid="movie-warpper"
        className="movie-backdrop"
        style={{ backgroundImage: `url('${getMovieImageUrl(backdrop_path)}')` }}
      >
        <div className="banner-backdrop">
          <Container className="py-5">
            <Row>
              <Col md="auto" xs="12" className="mb-4">
                <img
                  src={getMovieImageUrl(poster_path, "w300_and_h450_bestv2")}
                  alt={title}
                />
              </Col>
              <Col className="text-white">
                <h4 className="font-weight-normal">
                  <span className="font-weight-bold">{title}</span>&nbsp;(
                  {release_date.substr(0, 4)})
                </h4>
                <div className="d-flex">
                  {genres.map(({ id, name }, index) => {
                    return (
                      <div data-testid="genre-item" key={id}>
                        <Link className="text-white" to="/">
                          {name}
                        </Link>
                        {genres.length !== index + 1 ? (
                          <span className="pr-2">,</span>
                        ) : (
                          ""
                        )}
                      </div>
                    );
                  })}
                  <span className="px-2">-</span>
                  <span className="runtime">{runtime}m</span>
                </div>
                <h6>Overview</h6>
                <p>{overview}</p>
                {video && renderVideo()}
              </Col>
            </Row>
          </Container>
        </div>
      </div>
      <Container className="my-4">
        <h2 className="mb-4">Similar Movie</h2>
        <ListMovies movies={similarMovies} />
      </Container>

      {video && renderVideoModal()}
    </>
  );
}
