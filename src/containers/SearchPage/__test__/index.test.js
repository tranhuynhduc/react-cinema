import React from "react";
import { MovieContext } from "../../../contexts/MovieContext";
import { mount } from "enzyme";
import { SearchResultList, SearchPage } from "..";
import { MemoryRouter, Route } from "react-router-dom";
import { Loading } from "../../../components/Loading";
import mockMovie from "../../../mocks/mockMovie";
import mockMovies from "../../../mocks/mockMovies";

describe("Search Page", () => {
  const contextValue = {
    movie: mockMovie,
    search: jest.fn(),
    searchResults: mockMovies,
  };
  const customMount = (value = {}) => {
    return mount(
      <MemoryRouter initialEntries={[`/search?query="hi"`]}>
        <Route path="/search">
          <MovieContext.Provider value={{ ...contextValue, ...value }}>
            <SearchPage />
          </MovieContext.Provider>
        </Route>
      </MemoryRouter>
    );
  };

  test("should render search resutl list", () => {
    const wrapper = customMount();
    const searchResultList = wrapper.find(SearchResultList);

    expect(searchResultList).toHaveLength(1);
  });
});

describe("Search Result List", () => {
  test("should render Loading when data is null", () => {
    const wrapper = mount(<SearchResultList results={null} />);

    const loading = wrapper.find(Loading);
    expect(loading).toHaveLength(1);
  });

  test("should not render Loading when data is null", () => {
    const wrapper = mount(
      <MemoryRouter>
        <SearchResultList results={mockMovies} />
      </MemoryRouter>
    );

    const loading = wrapper.find(Loading);
    const movieShowed = mockMovies.filter(({ title }) => !!title);
    const reusltsItem = wrapper.find('[data-testid="result-wrapper"]');

    expect(loading).toHaveLength(0);
    expect(reusltsItem).toHaveLength(movieShowed.length);
  });
});
