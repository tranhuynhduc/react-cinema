import React, { useContext } from "react";
import MovieContextProvider, {
  MovieContext,
} from "../../../contexts/MovieContext";
import { mount } from "enzyme";
import MovieDetailsPage from "..";
import { MemoryRouter, Route } from "react-router-dom";
import mockData from "../../../components/ListMoves/__test__/mockData";
import { Loading } from "../../../components/Loading";
import mockMovie from "../../../mocks/mockMovie";
import { act } from "react-dom/test-utils";

const contextValue = {
  movie: mockData[0],
  getMovie: jest.fn(),
  clearMovie: jest.fn(),
  similarMovies: mockData,
};

describe("Movie Detail page", () => {
  let wrapper;

  const customMount = (value) => {
    return mount(
      <MemoryRouter initialEntries={["/movie/12345"]}>
        <Route path="/movie/:id">
          <MovieContext.Provider value={{ ...contextValue, ...value }}>
            <MovieDetailsPage />
          </MovieContext.Provider>
        </Route>
      </MemoryRouter>
    );
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  test("should render loading while loading movie", () => {
    wrapper = customMount({ movie: null });
    const loading = wrapper.find(Loading);
    expect(loading).toHaveLength(1);
  });

  test("should render movie when the data exists", () => {
    wrapper = customMount({ movie: mockMovie });
    const loading = wrapper.find(Loading);
    const movieWrapper = wrapper.find('[data-testid="movie-warpper"]');

    expect(loading).toHaveLength(0);
    expect(movieWrapper).toHaveLength(1);
  });

  test("should render video modal and button trailler when the video exists", () => {
    wrapper = customMount({ video: {} });
    const btnTrailer = wrapper.find('Button[data-testid="btn-trailer"]');
    let modalTrailler = wrapper.find('Modal[data-testid="modal-trailler"]');
    expect(btnTrailer).toHaveLength(1);
    expect(modalTrailler).toHaveLength(1);

    expect(modalTrailler.props().isOpen).toBeFalsy();
    btnTrailer.simulate("click");
    modalTrailler = wrapper.find('Modal[data-testid="modal-trailler"]');
    expect(modalTrailler.props().isOpen).toBeTruthy();
  });

  test("shoulld call cleanup after unmount componnt", () => {
    wrapper = customMount({ video: {} });
    expect(contextValue.clearMovie).toHaveBeenCalledTimes(0);
    wrapper.unmount();
    expect(contextValue.clearMovie).toHaveBeenCalledTimes(1);
  });
});

// describe("get movie", () => {
//   const response = {
//     results: mockMovie,
//   };
//   request.mockReturnValue(Promise.resolve(response));

//   let movies;
//   let getMovie;
//   let wrapper;
//   beforeEach(async () => {
//     const TestComponent = () => {
//       ({ movies, getMovie } = useContext(MovieContext));
//       return <button onClick={() => getMovie(12345)}></button>;
//     };
//     await act(async () =>
//       mount(
//         <MemoryRouter initialEntries={["/movie/12345"]}>
//           <Route path="/movie/:id">
//             <MovieContextProvider>
//               <MovieDetailsPage></MovieDetailsPage>
//             </MovieContextProvider>
//           </Route>
//         </MemoryRouter>
//       )
//     );
//   });
// });
