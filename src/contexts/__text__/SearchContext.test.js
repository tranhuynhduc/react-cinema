import React, { useContext } from "react";
import { SearchContext, SearchContextProvider } from "../SearchContext";
import request from "../../utils/request";
import mockMovies from "../../mocks/mockMovies";
import { act } from "react-dom/test-utils";
import { mount } from "enzyme";

jest.mock("../../utils/request");
const moviesResponse = {
  results: mockMovies,
};

describe("Search Context", () => {
  let searchResults;
  let search;
  let wrapper;
  let button;
  const TestComponent = () => {
    ({ searchResults, search } = useContext(SearchContext));

    return (
      <button
        data-testid="btn-search-movie"
        onClick={() => search(`&query="hero"`)}
      ></button>
    );
  };

  afterEach(() => {
    request.mockReset();
  });

  test("should get list movie when component is mounted", async () => {
    request.mockReturnValue(Promise.resolve(moviesResponse));
    await act(async () => {
      wrapper = mount(
        <SearchContextProvider>
          <TestComponent></TestComponent>
        </SearchContextProvider>
      );
    });

    const button = wrapper.find('[data-testid="btn-search-movie"]');
    await act(async () => {
      button.simulate("click");
    });

    expect(searchResults).toEqual(mockMovies);
  });
});
