import React from "react";
import {
  Navbar,
  NavLink,
  Container,
} from "reactstrap";

const Header = () => {
  return (
    <Navbar className="bg-blue main-nav" dark expand="md" sticky="top">
      <Container>
        <NavLink href="/" className="text-uppercase brand-name">
          React Cinema
        </NavLink>
      </Container>
    </Navbar>
  );
};

export default Header;
