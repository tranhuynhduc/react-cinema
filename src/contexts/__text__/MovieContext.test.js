import React, { useContext, useEffect } from "react";
import MovieContextProvider, { MovieContext } from "../MovieContext";
import { mount, ReactWrapper } from "enzyme";
import mockMovies from "../../mocks/mockMovies";
import request from "../../utils/request";
import mockMovie from "../../mocks/mockMovie";
import { act } from "react-dom/test-utils";

jest.mock("../../utils/request");
describe("Movie Context ", () => {
  const moviesResponse = {
    results: mockMovies,
  };

  let movies;
  let movie;
  let getMovie;
  let wrapper;
  let clearMovie;

  const TestComponent = () => {
    ({ movies, getMovie, clearMovie, movie } = useContext(MovieContext));
    const data = useContext(MovieContext);
    useEffect(() => {
      return () => {
        clearMovie();
      }
    }, [])
    return (
      <button
        data-testid="btn-get-movie"
        onClick={() => getMovie(12345)}
      ></button>
    );
  };

  afterEach(() => {
    request.mockReset();
  });

  test("should get list movie when component is mounted", async () => {
    request.mockReturnValue(Promise.resolve(moviesResponse));


    await act(async () => {
      wrapper = mount(
        <MovieContextProvider>
          <TestComponent></TestComponent>
        </MovieContextProvider>
      );
    });
    expect(movies).toEqual(mockMovies);
  });
  test("should return movie when get movie with id", async () => {
    // request.mockReturnValue(Promise.resolve(moviesResponse), Promise.resolve(mockMovie)).mockReturnValue(Promise.resolve(mockMovie)).mockReturnValue(Promise.resolve({
    // results: [{ key: "123"}]
    // })).mockReturnValue(Promise.resolve(moviesResponse));
    request
      .mockReturnValueOnce(Promise.resolve(moviesResponse))
      .mockReturnValueOnce(Promise.resolve(mockMovie))
      .mockReturnValueOnce(Promise.resolve({ results: [{ key: "123" }] }))
      .mockReturnValueOnce(Promise.resolve(moviesResponse));

    await act(async () => {
      wrapper = mount(
        <MovieContextProvider>
          <TestComponent></TestComponent>
        </MovieContextProvider>
      );
    });
    const button = wrapper.find('[data-testid="btn-get-movie"]');

    expect(movie).toBeFalsy();
    await act(async () => {
      button.simulate("click");
    });
    expect(movie).toEqual(mockMovie);
    wrapper.unmount();
  });

  test('should clear all data when unmount component', () => {

  })
});
