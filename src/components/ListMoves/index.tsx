import React from "react";
import Swiper from "react-id-swiper";
import { IMovie } from "../../interfaces/MovieInterface";
import { CardImg, CardBody, CardText, CardTitle } from "reactstrap";
import { Link } from "react-router-dom";
import { getMovieImageUrl } from "../../utils/movieHelper";
import { Loading } from "../Loading";

export const ListMovies = ({ movies }) => {
  if (!movies || !movies.length) {
    return <Loading />;
  }

  const params = {
    slidesPerView: "auto",

    spaceBetween: 25,
  };
  return (
    <Swiper data-testid="swiper" {...params}>
      {movies.map(
        (
          {
            title,
            id,
            poster_path: backdropPath,
            release_date: releaseDate,
          }: IMovie,
          index
        ) => {
          return (
            <div key={id} style={{ width: 150 }} data-testid="movie-item">
              <Link className="popular-movie" to={`/movie/${id}`}>
                <div className="hover-image">
                  <CardImg
                    style={{ minHeight: 225, borderRadius: 16 }}
                    top
                    src={getMovieImageUrl(backdropPath)}
                    alt={title}
                    data-testid="card-image"
                  />
                </div>
                <CardBody className="p-1 text-left">
                  <CardTitle
                    className="font-weight-bold m-0 movie-title"
                    title={title}
                  >
                    {title}
                  </CardTitle>
                  <CardText>{releaseDate}</CardText>
                </CardBody>
              </Link>
            </div>
          );
        }
      )}
    </Swiper>
  );
};

export default ListMovies;
