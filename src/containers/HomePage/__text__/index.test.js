import React from "react";
import HomePage from "../index";
import { mount } from "enzyme";
import routerDom, { MemoryRouter } from "react-router-dom";

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

const mockHistory = {
  push: jest.fn(),
};

describe("App Component", () => {
  let wrapper;
  let input;
  const searchText = "searchText";
  beforeEach(() => {
    wrapper = mount(
      <MemoryRouter initialEntries={["/homepage"]}>
        <HomePage />
      </MemoryRouter>
    );

    input = wrapper.find('input[name="search"]');
  });

  afterEach(() => {
    jest.clearAllMocks();
  })
  test("App should render", () => {
    expect(wrapper).not.toBeNull();
  });

  test("Update search key when user input", () => {
    const button = wrapper.find('Button[data-test="btn-search"]');

    input.simulate("change", { target: { value: searchText } });

    expect(input.instance().value).toEqual(searchText);
  });
  test("should navigate when have the input is filled with search text", () => {
    const button = wrapper.find('Button[data-test="btn-search"]');
    jest.spyOn(routerDom, "useHistory").mockReturnValue(mockHistory);

    button.simulate("click");
    expect(mockHistory.push).not.toHaveBeenCalled();

    input.simulate("change", { target: { value: searchText } });

    expect(input.instance().value).toEqual(searchText);

    button.simulate("click");

    expect(mockHistory.push).toHaveBeenCalled();
  });
  test("should navigate when have the input is filled with search text and press enter key", () => {
    const button = wrapper.find('Button[data-test="btn-search"]');
    jest.spyOn(routerDom, "useHistory").mockReturnValue(mockHistory);

    button.simulate("click");

    input.simulate("keydown", { key: 'Space' } );
    expect(mockHistory.push).not.toHaveBeenCalled();

    input.simulate("change", { target: { value: searchText } });
    input.simulate("keydown", { key: 'Enter' } );

    expect(input.instance().value).toEqual(searchText);

    button.simulate("click");

    expect(mockHistory.push).toHaveBeenCalled();
  });
});
