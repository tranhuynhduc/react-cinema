import React from "react";
import App from "./App";
import { mount } from "enzyme";
import {MemoryRouter, } from "react-router-dom";

describe("App Component", () => {
  let wrapper;
  let input;
  beforeEach(() => {
    wrapper = mount(
      <MemoryRouter>
        <App />
      </MemoryRouter>
    );

    input = wrapper.find('input[name="search"]');
  });
  test("App should render", () => {
    expect(wrapper).not.toBeNull();
  });

  test("Update search key when user input", () => {
    const searchText = "searchText";

    wrapper.update();
    input.simulate("change", { target: { value: searchText } });
    input = wrapper.find('input[name="search"]');

    expect(input.instance().value).toEqual(searchText);
  });

});
