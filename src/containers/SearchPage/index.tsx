import React, { useContext, useEffect } from "react";
import { SearchContext } from "../../contexts/SearchContext";
import { useLocation, Link } from "react-router-dom";
import {
  Container,
  Row,
  Col,
  CardImg,
  CardBody,
  CardTitle,
  CardText,
} from "reactstrap";
import { IMovie } from "../../interfaces/MovieInterface";
import { getMovieImageUrl } from "../../utils/movieHelper";
import { Loading } from "../../components/Loading";

export const SearchPage = () => {
  const { search, searchResults } = useContext(SearchContext);

  const query = useLocation().search.replace("?", "&");
  console.log("query", query);
  useEffect(() => {
    search(query);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [query]);

  return (
    <Container className="my-4">
      <Row>
        {/* <Col xs="3"></Col> */}
        <Col>
          <h5>Search Result</h5>
          <SearchResultList results={searchResults} />
        </Col>
      </Row>
    </Container>
  );
};

export const SearchResultList = ({ results }) => {
  if (!results) return <Loading />;

  return results.map(
    ({
      title,
      overview,
      id,
      poster_path: backdropPath,
      release_date: releaseDate,
    }: IMovie) => {
      return (
        title && (
          <div key={id} className="mb-4 result-wrapper" data-testid="result-wrapper">
            <Row noGutters>
              <Col xs="auto">
                <Link to={`/movie/${id}`}>
                  <CardImg
                    style={{ maxWidth: 84 }}
                    top
                    src={getMovieImageUrl(backdropPath, "w94_and_h141_bestv2")}
                    alt={title}
                  />
                </Link>
              </Col>
              <Col>
                <CardBody className="px-3 py-2">
                  <Link to={`/movie/${id}`}>
                    <CardTitle
                      className="font-weight-bold m-0 movie-title"
                      title={title}
                    >
                      {title}
                    </CardTitle>
                  </Link>
                  <CardText className="mb-2">{releaseDate}</CardText>
                  <h6 className="ellipsis-2 font-weight-normal">{overview}</h6>
                </CardBody>
              </Col>
            </Row>
          </div>
        )
      );
    }
  );
};
