import React from 'react'
// import { Circle } from 'react-spinners-css';
import { Spinner } from 'reactstrap';
export const Loading = () => {
  return (
    <div className="loading-icon">
      <Spinner />
    </div>
  )
}
