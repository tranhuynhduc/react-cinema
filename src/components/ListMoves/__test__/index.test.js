import React from "react";
import { mount } from "enzyme";
import ListMovies from "..";
import moviesData from "./mockData";
import { MemoryRouter } from "react-router-dom";

describe("List Movie", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(
      <MemoryRouter>
        <ListMovies movies={moviesData} />
      </MemoryRouter>
    );
  });
  test("should no render if api don't return any movie", async () => {
    wrapper = mount(
      <MemoryRouter>
        <ListMovies movies={[]} />
      </MemoryRouter>
    );
    const movies = wrapper.find('[data-testid="movie-item"]');
    expect(movies).toHaveLength(0);
  });
  test("render without any issues", async () => {
    wrapper = mount(
      <MemoryRouter>
        <ListMovies movies={moviesData} />
      </MemoryRouter>
    );
    const movies = wrapper.find('[data-testid="movie-item"]');
    expect(movies).toHaveLength(moviesData.length);
  });

});
